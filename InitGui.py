# ***************************************************************************
# *   Copyright (c) 2021 David Eccles (gringer)                             *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

# Based on examples at : https://www.freecadweb.org/wiki/Workbench_creation

# Version 01.00 (2022-01-01)
# flake

class SnowflakeWorkbench(Workbench):
    MenuText = "Snowflake"
    ToolTip = "A workbench for generating recursively-defined snowflakes"
    Icon = str(FreeCAD.getUserAppDataDir()) + "Mod/Snowflake/Resources/Icons/snowflake.svg"

    def Initialize(self):
        """This function is executed when FreeCAD starts"""
        import snowflake # import here all the needed files that create your FreeCAD commands
        self.list = ["Crystal", "Tree", "Snowflake"] # A list of command names created in the line above
        self.appendToolbar("Snowflake",self.list) # creates a new toolbar with your commands
        self.appendMenu("Snowflake",self.list) # creates a new menu

    def Activated(self):
        """This function is executed when the workbench is activated"""
        return

    def Deactivated(self):
        """This function is executed when the workbench is deactivated"""
        return

    def ContextMenu(self, recipient):
        """This is executed whenever the user right-clicks on screen"""
        # "recipient" will be either "view" or "tree"
        self.appendContextMenu("Snowflake",self.list) # add commands to the context menu

    def GetClassName(self):
        # this function is mandatory if this is a full python workbench
        return "Gui::PythonWorkbench"

Gui.addWorkbench(SnowflakeWorkbench())
