import FreeCAD as App
import FreeCADGui
import Part
import math
import sys
import ast
import copy
import random
from FreeCAD import Base

def getWorkbenchFolder():
    import os.path
    from os import path
    recommended_folders = ['Mod/Snowflake', 'Mod/snowflake']
    basedir = str(App.getUserAppDataDir())
    folder = ""
    for tryfolder in recommended_folders:
            if path.exists(basedir + tryfolder):
                    folder = basedir + tryfolder
                    return folder
    return ""

def crystalVertexes(width, length, z):
  vertexes = []
  radius = width / 2
  if radius != 0 :
    for i in range(7): ## a six-sided pyramid, reflected and duplicated in the Z axis
      angle = i * math.pi / 3 + 2*math.pi/3
      if((i % 6) > 2):
        vertex = (radius * math.cos(angle) + length, radius * math.sin(angle), z)
      else:
        vertex = (radius * math.cos(angle), radius * math.sin(angle), z)
      vertexes.append(vertex)
  else:
    vertex = (0,0,z)
    vertexes.append(vertex)
  return vertexes
  
def makeCrystal(cWidth, cLength):
  faces = []
  if(cWidth == 0):
    App.Console.PrintMessage("Crystal width is zero\n")
  else:
    vertexes_top = crystalVertexes(width=cWidth*math.cos(math.pi/3),
                                   length=cLength,
                                   z=cWidth/4 * math.sin(math.pi/3))
    vertexes_mid = crystalVertexes(width=cWidth, length=cLength, z=0)
    vertexes_bottom = crystalVertexes(width=cWidth*math.cos(math.pi/3),
                                      length=cLength,
                                      z=-cWidth/4 * math.sin(math.pi/3))
    ##App.Console.PrintMessage("Bottom:" + str(vertexes_bottom) + "\n");
    ##App.Console.PrintMessage("Mid:" + str(vertexes_mid) + "\n");
    ##App.Console.PrintMessage("Top:" + str(vertexes_top) + "\n");
    ## Add top face
    polygon_top = Part.makePolygon(vertexes_top)
    face_top = Part.Face(polygon_top)
    faces.append(face_top)
    ## Add bottom face
    polygon_bottom = Part.makePolygon(vertexes_bottom)
    face_bottom = Part.Face(polygon_bottom)
    faces.append(face_bottom)
    ## Add side faces
    for i in range(6):
      ## Top
      vertexes_sideTop = [vertexes_mid[i], vertexes_mid[(i+1) % 6],
        vertexes_top[(i+1) % 6], vertexes_top[i], vertexes_mid[i]]
      polygon_sideTop = Part.makePolygon(vertexes_sideTop)
      faces.append(Part.Face(polygon_sideTop))
      ## Bottom
      vertexes_sideBottom = [vertexes_bottom[i], vertexes_bottom[(i+1) % 6],
        vertexes_mid[(i+1) % 6], vertexes_mid[i], vertexes_bottom[i]]
      polygon_sideBottom = Part.makePolygon(vertexes_sideBottom)
      faces.append(Part.Face(polygon_sideBottom))
    shell = Part.makeShell(faces)
    solid = Part.makeSolid(shell)
    return solid

def aM(x, vMin, vMax = 1):
  return (x * (vMax - vMin) + vMin)

class Crystal:
  def __init__(self, obj, cWidth = 2, cLength = 10):
    obj.addProperty("App::PropertyLength","Width","Crystal","Width of the crystal").Width=cWidth
    obj.addProperty("App::PropertyLength","Length","Crystal","Length of the crystal").Length=cLength
    obj.Proxy = self

  def execute(self, obj):
    obj.Shape = makeCrystal(cWidth = float(obj.Width), cLength =float(obj.Length))

class Tree:
  def __init__(self, obj, bWidth = 8, bLength = 20, seed=-1, cCount = 6,
      structure=""):
      # structure="[[0,0.2,0.5], [[0.5],[[0,0.2,1]]]]"):
    if(seed == -1):
      random.seed()
      seed=random.randint(0, 10**6)
    obj.addProperty("App::PropertyLength","BaseWidth","Tree","Width of the primary crystals").BaseWidth=bWidth
    obj.addProperty("App::PropertyLength","BaseLength","Tree","Length of the primary crystals").BaseLength=bLength
    obj.addProperty("App::PropertyInteger","Seed","Tree","Random number seed to generate tree").Seed=seed
    obj.addProperty("App::PropertyInteger","CrystalCount","Tree","Number of crystals to create in tree").CrystalCount=cCount
    obj.addProperty("App::PropertyString","Structure","Tree","Description of the tree structure (overrides seed)").Structure=structure
    obj.Proxy = self
    
  def makeStructure(baseLength, baseWidth, minThickness=0.4, branchMin=0.3,
      numCrystals=4, descentProb=0.8, lastDescent=False, scale=1, maxDist=0, res=[]):
    if(numCrystals <= 0):
      return(res)
    if((not lastDescent) and (maxDist > branchMin) and ((baseWidth * scale) > minThickness) and 
        (random.uniform(0, 1) < descentProb)):
      pos = round(aM(random.uniform(0,1), branchMin, maxDist),3)
      newScale = scale * (1-pos)
      descentCrystals = random.randint(1,numCrystals) 
      newStruc = Tree.makeStructure(baseLength=baseLength, baseWidth=baseWidth,
              minThickness=minThickness, branchMin=branchMin,
              numCrystals=descentCrystals, descentProb=descentProb, lastDescent=True,
              scale=newScale, maxDist=0, res=[])
      if(len(newStruc) > 0):
        subTree = [[pos]] + [newStruc]
        res = res + [subTree]
      numCrystals = numCrystals - descentCrystals
    if(numCrystals <= 0):
      return res
    else:
      nextPos = round(random.uniform(0, 1) * maxDist, 3)
      nextLen = round(aM(random.uniform(0, 1), branchMin), 3)
      newDist = round(min(1, max(maxDist, nextPos + aM(random.uniform(0,1), branchMin))), 3)
      nextWidth = round(aM(random.uniform(0, 1), vMin=minThickness/scale, vMax=baseWidth) / baseWidth, 3)
      res = res + [[nextPos, nextWidth, nextLen]]
      return Tree.makeStructure(baseLength=baseLength, baseWidth=baseWidth,
              minThickness=minThickness, branchMin=branchMin,
              numCrystals=numCrystals-1, descentProb=descentProb, lastDescent=False,
              scale=scale, maxDist=newDist, res=res)
    
  def makeTree(structure, bWidth, bLength, scale = 1.0):
    # App.Console.PrintMessage("Structure: " + str(structure) + "\n");
    treeSeed = None
    for c in structure: # fetch structure components
      if(len(c) == 3): # create a crystal
        # App.Console.PrintMessage("c [crystal]: " + str(c) + "\n");
        crystal = makeCrystal(cWidth = c[1] * bWidth * scale, cLength = c[2] * bLength * scale)
        crystal.Placement = App.Placement(App.Vector(c[0] * bLength * scale,0,0), App.Rotation(App.Vector(0,0,1),0))
        if not treeSeed:
          treeSeed = crystal
        else:
          treeSeed = treeSeed.fuse(crystal)
      elif(len(c) == 2): # create a mirrored split
        newPos = c[0][0]
        # App.Console.PrintMessage("c [tree]: " + str(c) + "\n");
        # App.Console.PrintMessage("newPos: " + str(newPos) + "\n");
        subTree1 = Tree.makeTree(structure = c[1], bWidth = bWidth, bLength = bLength, scale = scale * (1 - newPos))
        subTree2 = copy.deepcopy(subTree1)
        subTree1.Placement.Rotation = App.Rotation(App.Vector(0,0,1),-60)
        subTree2.Placement.Rotation = App.Rotation(App.Vector(0,0,1),60)
        subTree = subTree1.fuse(subTree2)
        subTree.Placement = App.Placement(App.Vector(newPos * bLength * scale, 0, 0),App.Rotation(App.Vector(0,0,1),0))
        if not treeSeed:
          treeSeed = subTree
        else:
          treeSeed = treeSeed.fuse(subTree)
      else:
        App.Console.PrintMessage("Found a tree structure element with %d items (expecting 2 or 3 items)\n" % len(c));
    if not treeSeed:
      treeSeed = makeCrystal(cWidth = bWidth * scale, cLength = 0)
    return treeSeed

  def execute(self, obj):
    bWidth = float(obj.BaseWidth)
    bLength = float(obj.BaseLength)
    seed = int(obj.Seed)
    structure = str(obj.Structure)
    cCount = int(obj.CrystalCount)
    flakeSeed = makeCrystal(cWidth = 0.1 * bWidth, cLength = 0)
    if (not structure) or (not structure.strip()):
      random.seed(seed)
      structure = str(Tree.makeStructure(baseLength=bLength, baseWidth=bWidth, numCrystals = cCount, descentProb = 0.8))
      obj.Structure = structure
      # App.Console.PrintMessage("generated structure: " + str(structure) + "\n");
    if structure and structure.strip():
      structure = ast.literal_eval(structure) # convert to nested list representation
    tree = Tree.makeTree(structure, bWidth, bLength)
    obj.Shape = tree

class Snowflake:
  def __init__(self, obj, bWidth = 8, bLength = 20, seed=-1, cCount = 6, structure=""):
    if(seed == -1):
      random.seed()
      seed=random.randint(0, 10**6)
    obj.addProperty("App::PropertyLength","BaseWidth","Snowflake","Width of the primary crystals").BaseWidth=bWidth
    obj.addProperty("App::PropertyLength","BaseLength","Snowflake","Length of the primary crystals").BaseLength=bLength
    obj.addProperty("App::PropertyInteger","Seed","Snowflake","Random number seed to generate snowflake").Seed=seed
    obj.addProperty("App::PropertyInteger","CrystalCount","Tree","Number of crystals to create in tree").CrystalCount=cCount
    obj.addProperty("App::PropertyString","Structure","Snowflake","Description of the flake structure (overrides seed)").Structure=structure
    obj.Proxy = self

  def execute(self, obj):
    bWidth = float(obj.BaseWidth)
    bLength = float(obj.BaseLength)
    seed = int(obj.Seed)
    structure = str(obj.Structure)
    cCount = int(obj.CrystalCount)
    flakeSeed = makeCrystal(cWidth = 0.1 * bWidth, cLength = 0)
    if (not structure) or (not structure.strip()):
      random.seed(seed)
      structure = str(Tree.makeStructure(baseLength=bLength, baseWidth=bWidth, numCrystals = cCount, descentProb = 0.8))
      obj.Structure = structure
    if structure and structure.strip():
      structure = ast.literal_eval(structure) # convert to nested list representation
    tree = Tree.makeTree(structure, bWidth, bLength)
    for i in range(6):
      treeCopy = copy.deepcopy(tree)
      #crystal = makeCrystal(cWidth = bWidth, cLength = bLength)
      treeCopy.Placement = App.Placement(App.Vector(0,0,0), App.Rotation(App.Vector(0,0,1), 60 * i))
      flakeSeed = flakeSeed.fuse(treeCopy)
    obj.Shape = flakeSeed

class ViewProviderCrystal:
    obj_name = "Crystal"
    def __init__(self, obj, obj_name):
        obj.Proxy = self
    def attach(self, obj):
        return
    def updateData(self, fp, prop):
        return
    def getDisplayModes(self,obj):
        return "As Is"
    def getDefaultDisplayMode(self):
        return "As Is"
    def setDisplayMode(self,mode):
        return "As Is"
    def onChanged(self, vobj, prop):
        pass
    def getIcon(self):
        return getWorkbenchFolder() + "/Resources/Icons/crystal.svg"
    def __getstate__(self):
        return None
    def __setstate__(self,state):
        return None

class ViewProviderSnowflake:
    obj_name = "Snowflake"
    def __init__(self, obj, obj_name):
        obj.Proxy = self
    def attach(self, obj):
        return
    def updateData(self, fp, prop):
        return
    def getDisplayModes(self,obj):
        return "As Is"
    def getDefaultDisplayMode(self):
        return "As Is"
    def setDisplayMode(self,mode):
        return "As Is"
    def onChanged(self, vobj, prop):
        pass
    def getIcon(self):
        return getWorkbenchFolder() + "/Resources/Icons/snowflake.svg"
    def __getstate__(self):
        return None
    def __setstate__(self,state):
        return None

class ViewProviderTree:
    obj_name = "Tree"
    def __init__(self, obj, obj_name):
        obj.Proxy = self
    def attach(self, obj):
        return
    def updateData(self, fp, prop):
        return
    def getDisplayModes(self,obj):
        return "As Is"
    def getDefaultDisplayMode(self):
        return "As Is"
    def setDisplayMode(self,mode):
        return "As Is"
    def onChanged(self, vobj, prop):
        pass
    def getIcon(self):
        return getWorkbenchFolder() + "/Resources/Icons/tree.svg"
    def __getstate__(self):
        return None
    def __setstate__(self,state):
        return None

class CrystalCommand:
  def GetResources(self):
    return {'Pixmap'  : getWorkbenchFolder() + "/Resources/Icons/crystal.svg",
                'Accel' : "Shift+C",
                'MenuText': "Crystal",
                'ToolTip' : "Generate an ice Crystal with a given length and width"}
  def Activated(self):
    obj=App.ActiveDocument.addObject("Part::FeaturePython","Crystal")
    Crystal(obj)
    ViewProviderCrystal(obj.ViewObject, "Crystal")
    App.ActiveDocument.recompute()
    FreeCADGui.SendMsgToActiveView("ViewFit")
    return

  def IsActive(self):
    if App.ActiveDocument == None:
      return False
    else:
      return True

class SnowflakeCommand:
  def GetResources(self):
    return {'Pixmap'  : getWorkbenchFolder() + "/Resources/Icons/snowflake.svg",
                'Accel' : "Shift+S",
                'MenuText': "Snowflake",
                'ToolTip' : "Create a rotationally-symmetric snowflake with six branches"}
  def Activated(self):
    obj=App.ActiveDocument.addObject("Part::FeaturePython","Snowflake")
    Snowflake(obj)
    ViewProviderSnowflake(obj.ViewObject, "Snowflake")
    App.ActiveDocument.recompute()
    FreeCADGui.SendMsgToActiveView("ViewFit")
    return
  def IsActive(self):
    if App.ActiveDocument == None:
      return False
    else:
      return True

class TreeCommand:
  def GetResources(self):
    return {'Pixmap'  : getWorkbenchFolder() + "/Resources/Icons/tree.svg",
                'Accel' : "Shift+T",
                'MenuText': "Tree",
                'ToolTip' : "Create a recursive branching tree structure (i.e. one snowflake arm)"}
  def Activated(self):
    obj=App.ActiveDocument.addObject("Part::FeaturePython","Tree")
    Tree(obj)
    ViewProviderTree(obj.ViewObject, "Tree")
    App.ActiveDocument.recompute()
    FreeCADGui.SendMsgToActiveView("ViewFit")
    return
  def IsActive(self):
    if App.ActiveDocument == None:
      return False
    else:
      return True


FreeCADGui.addCommand('Crystal',CrystalCommand())
FreeCADGui.addCommand('Tree',TreeCommand())
FreeCADGui.addCommand('Snowflake',SnowflakeCommand())
